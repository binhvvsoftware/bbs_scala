
name := """BBS"""
organization := "com.example"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.12.6"

libraryDependencies += filters

// ScalaTest and Play framework
// https://github.com/playframework/scalatestplus-playst
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % Test
// specs2 for testing
libraryDependencies += specs2 % Test

// extension for Google Guice
libraryDependencies += guice
// extension for Play evolutions (similar to Flyway)
libraryDependencies += evolutions

// scalikeJDBC dependencies
// http://scalikejdbc.org/
libraryDependencies += "org.scalikejdbc" %% "scalikejdbc" % "3.2.2"
libraryDependencies += "org.scalikejdbc" %% "scalikejdbc-config" % "3.2.0"
libraryDependencies += "org.scalikejdbc" %% "scalikejdbc-play-dbapi-adapter" % "2.6.0-scalikejdbc-3.2" // for evolutions
//libraryDependencies += "com.h2database" % "h2" % "1.4.197" <-- this is just one database to use, equivalent to MySQL

// mySQL dependencies
libraryDependencies += "mysql" % "mysql-connector-java" % "8.0.11"

// Password encryption
libraryDependencies += "org.mindrot" % "jbcrypt" % "0.4"

libraryDependencies ++= Seq(
  "org.skinny-framework" %% "skinny-orm"      % "3.0.0",
  "com.h2database"       %  "h2"              % "1.4.+",
  "ch.qos.logback"       %  "logback-classic" % "1.1.+"
)


