# Posts schema creation

# --- !Ups

CREATE TABLE user (
    id int(11) AUTO_INCREMENT PRIMARY KEY,
    email varchar(100) COLLATE utf8_unicode_ci NOT NULL,
    password varchar(100) COLLATE utf8_unicode_ci NOT NULL,
    status tinyint(1) NOT NULL,
    created_date datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `user` (`id`, `email`, `password`, `status`, `created_date`) VALUES
(1, 'binhvvsoftware@gmail.com', 'admin@123', 0, '2019-04-05 00:00:00');


# --- !Downs

DROP TABLE IF EXISTS user;
