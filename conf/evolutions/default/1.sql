# Posts schema creation

# --- !Ups

CREATE TABLE post
(
    id           int(11)                              AUTO_INCREMENT PRIMARY KEY,
    title        varchar(255) COLLATE utf8_unicode_ci NOT NULL,
    content      text COLLATE utf8_unicode_ci         NOT NULL,
    status       tinyint(1)                           NOT NULL,
    user_id      int(11)                              NOT NULL,
    created_date datetime                             NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;


INSERT INTO `post` (`id`, `title`, `content`, `status`, `user_id`, `created_date`) VALUES
(1, 'Who Am i', 'What is your name? who am i!!!', 1, 1, '2019-04-05 00:00:00'),
(2, 'Mr.Binh', 'What is your name', 0, 1, '2019-04-05 00:00:00');

# --- !Downs

DROP TABLE IF EXISTS posts;