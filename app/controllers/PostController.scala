package controllers

import javax.inject._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import play.api.data.validation.Constraints._
import services.PostService

case class PostForm(title: String, content: String)

object PostForm {
	val postForm = Form(
		mapping(
			"title" -> nonEmptyText,
			"content" -> nonEmptyText
		)(PostForm.apply)(PostForm.unapply)
	)
}

@Singleton
class PostController @Inject() (cc: ControllerComponents, postService: PostService)
  extends AbstractController(cc) with play.api.i18n.I18nSupport {

  /**
    * List all posts
    * @return Home page
    */
  def listPost = Action { implicit request =>
    val listPost = postService.listPost
    Ok(views.html.post.listPost(listPost))
  }

  /**
    * A page to create post
    * @return Post form page
    */
  def createPost() = Action {  implicit request =>
    Ok(views.html.post.createPost(PostForm.postForm))
  }

  /**
    * Show details of a post
    * @param id ID of the post
    * @return Post page if valid, otherwise Not Found page
    */
  def viewPostById(id: Int) = Action {  implicit request =>
    val listPost = postService.listById(id)
    Ok(views.html.post.viewPost(listPost.toList))
  }

  /**
    * Delete one post
    * @return Home page
    */
  def deletePost(id: Int) = Action {  implicit request =>
    postService.deletePost(id)
    Redirect(routes.PostController.listPost)
  }

  /**
    * Insert post to database
    * @return Post form page with creation result
    */
  def savePost() = Action { implicit request =>
    PostForm.postForm.bindFromRequest.fold(
      formWithErrors => {
        BadRequest(views.html.post.createPost(formWithErrors))
      },
      dataPost => {
        postService.insertPost(dataPost.title, dataPost.content)
        Redirect(routes.PostController.listPost)
      })
  }
}
