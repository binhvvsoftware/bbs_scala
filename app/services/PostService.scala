package services

import javax.inject._
import com.google.inject.ImplementedBy
import repository.PostRepo
import model.Post

@ImplementedBy(classOf[PostService])
trait PostServiceApi{
  def listPost: List[Post]
  def insertPost(title: String, content: String): Unit
  def listById(id: Int): Seq[Post]
  def deletePost(id: Int): Unit
}

class PostService @Inject()(post: PostRepo) extends PostServiceApi{

  override def listPost: List[Post] = {
    post.listPost
  }

  override def insertPost(title: String, content: String): Unit = {
    post.insertPost(title, content)
  }

  override def listById(id: Int): Seq[Post] = {
    post.listById(id)
  }

  override def deletePost(id: Int): Unit = {
    post.deletePost(id)
  }
}
