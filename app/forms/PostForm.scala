package forms

import play.api.data._
import play.api.data.Forms._
import play.api.data.validation.Constraints._

case class PostForm(title: String, content: String)

object PostForm {
  val postForm = Form(
    mapping(
      "title" -> nonEmptyText,
      "content" -> nonEmptyText
    )(PostForm.apply)(PostForm.unapply)
  )
}
