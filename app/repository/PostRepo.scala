package repository

import model.Post
import scalikejdbc._
import skinny.orm._

object Post extends SkinnyCRUDMapper[Post] {
  override lazy val defaultAlias = createAlias("p")
  override def extract(rs: WrappedResultSet, n: ResultName[Post]): Post = new Post(
    id = rs.get(n.id),
    userId = rs.get(n.userId),
    title = rs.get(n.title),
    content = rs.get(n.content),
    status = rs.get(n.status),
    createdDate = rs.get(n.createdDate))
}

class PostRepo {

  private implicit val DBSession: DBSession = AutoSession

  def listPost: List[Post] = Post.findAll()

  def listById(id: Int): Seq[Post] = {
    Post.findAllBy(sqls.eq(Post.defaultAlias.id, id))
  }

  def insertPost(title: String, content: String): Unit = {
    Post.createWithAttributes('title -> title, 'content -> content, 'status -> 0, 'user_id->1 )
  }

  def deletePost(id: Int): Unit = {
    Post.deleteById(id)
  }
}