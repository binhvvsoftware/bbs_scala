package model

import org.joda.time.DateTime

case class Post(id: Int, userId: Int, title: String, content: String, status: Boolean, createdDate: DateTime) {}
